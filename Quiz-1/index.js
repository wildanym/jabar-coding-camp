// soal 1 - Function Penghitung Jumlah Kata
function jumlah_kata(kalimat) {
	var hasil = kalimat
		.split(" ")
		.map(function (kata) {
			return kata;
		})
		.filter(function (saring) {
			return saring != "";
		});
	console.log(hasil.length);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = " Saya Iqbal";
var kalimat_3 = " Saya Muhammad Iqbal Mubarok ";

jumlah_kata(kalimat_1);
jumlah_kata(kalimat_2);
jumlah_kata(kalimat_3);

// soal 2 - Function Penghasil Tanggal Hari Esok
var tanggal = 29;
var bulan = 2;
var tahun = 2020;
function next_date(tgl, bln, thn) {
	if (tanggal > 31) {
		return console.log("Tanggal yang dimasukan Salah !");
	}
	switch (bln) {
		case 1:
			bulanstr = " Januari ";
			hari = 31;
			break;
		case 2:
			bulanstr = " Februari ";
			if ((0 == tahun % 4 && 0 != tahun % 100) || 0 == tahun % 400) {
				hari = 29;
			} else {
				hari = 28;
			}
			if (tanggal > hari) {
				return console.log("Tanggal yang dimasukan Salah !");
			}
			break;
		case 3:
			bulanstr = " Maret ";
			hari = 31;
			break;
		case 4:
			bulanstr = " April ";
			hari = 30;
			break;
		case 5:
			bulanstr = " Mei ";
			hari = 31;
			break;
		case 6:
			bulanstr = " Juni ";
			hari = 30;
			break;
		case 7:
			bulanstr = " Juli ";
			hari = 31;
			break;
		case 8:
			bulanstr = " Agustus ";
			hari = 31;
			break;
		case 9:
			bulanstr = " September ";
			hari = 30;
			break;
		case 10:
			bulanstr = " Oktober ";
			hari = 31;
			break;
		case 11:
			bulanstr = " November ";
			hari = 30;
			break;
		case 12:
			bulanstr = " Desember ";
			hari = 31;
			break;
		default:
			bulanstr = "( bulan yang anda masukan salah )";
	}
	if (bln == 12 && tgl == hari) {
		next_date(1, 1, thn + 1);
	} else if (tgl == hari && bln != 12) {
		next_date(1, bln + 1, thn);
	} else {
		if (tgl == tanggal) {
			tgl += 1;
		}
		console.log(tgl, bulanstr, thn);
	}
}

next_date(tanggal, bulan, tahun);
