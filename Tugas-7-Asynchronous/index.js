var readBooks = require("./callback.js");

var books = [
	{ name: "LOTR", timeSpent: 3000 },
	{ name: "Fidas", timeSpent: 2000 },
	{ name: "Kalkulus", timeSpent: 4000 },
	{ name: "komik", timeSpent: 1000 },
];

// Tulis code untuk memanggil function readBooks di sini
function tampil(time, book, i = 0) {
	function callback(sisaWaktu) {
		if (sisaWaktu > 0) {
			i++;
			tampil(sisaWaktu, books, i);
		}
	}
	if (i < books.length) {
		readBooks(time, book[i], callback);
	}
}
tampil(10000, books);
