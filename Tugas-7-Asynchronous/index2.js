var readBooksPromise = require("./promise.js");

var books = [
	{ name: "LOTR", timeSpent: 3000 },
	{ name: "Fidas", timeSpent: 2000 },
	{ name: "Kalkulus", timeSpent: 4000 },
	{ name: "komik", timeSpent: 1000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
function tampil(time, book, i) {
	if (i < books.length) {
		readBooksPromise(time, book[i])
			.then((sisaWaktu) => {
				if (sisaWaktu >= 0) {
					tampil(sisaWaktu, book, i);
				}
			})
			.catch((error) => {
				console.log("waktu habis");
			});
		i++;
	}
}
tampil(10000, books, 0);
