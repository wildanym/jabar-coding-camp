// soal 1
var nilai = 90;
var indeks;

if (nilai >= 85) {
	indeks = "A";
} else if (nilai >= 75 && nilai < 85) {
	indeks = "B";
} else if (nilai >= 65 && nilai < 75) {
	indeks = "C";
} else if (nilai >= 55 && nilai < 65) {
	indeks = "D";
} else {
	indeks = "E";
}
console.log("Nilai " + nilai + " indeksnya adalah " + indeks);

// soal 2
var tanggal = 17;
var bulan = 1;
var tahun = 1995;

switch (bulan) {
	case 1:
		bulan = " Januari ";
		break;
	case 2:
		bulan = " Februari ";
		break;
	case 3:
		bulan = " Maret ";
		break;
	case 4:
		bulan = " April ";
		break;
	case 5:
		bulan = " Mei ";
		break;
	case 6:
		bulan = " Juni ";
		break;
	case 7:
		bulan = " Juli ";
		break;
	case 8:
		bulan = " Agustus ";
		break;
	case 9:
		bulan = " September ";
		break;
	case 10:
		bulan = " Oktober ";
		break;
	case 11:
		bulan = " November ";
		break;
	case 12:
		bulan = " Desember ";
		break;
	default:
		bulan = "( bulan yang anda masukan salah )";
}

console.log(tanggal + bulan + tahun);

// soal 3
var cetak = "";
var n = 7;
for (var i = 0; i < n; i++) {
	for (var j = 0; j <= i; j++) {
		cetak += "#";
	}
	cetak += "\n";
}
console.log(cetak);

// soal 4
var m = 10;
var cetak = "";
for (var i = 1; i <= m; i++) {
	if (i % 3 === 1) {
		cetak += i + " - " + "I Love Programming\n";
	} else if (i % 3 === 2) {
		cetak += i + " - " + "I Love Javascript\n";
	} else if (i % 3 === 0) {
		cetak += i + " - " + "I Love VueJs\n";
		for (var a = 0; a < i; a++) {
			cetak += "=";
		}
		cetak += "\n";
	}
}
console.log(cetak);
