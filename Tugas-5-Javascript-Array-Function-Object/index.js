// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort().forEach(function (hewan) {
	console.log(hewan);
});

// soal 2
var data = {
	name: "John",
	age: 30,
	address: "Jalan Pelesiran",
	hobby: "Gaming",
};
function introduce(data) {
	return (
		"Nama saya " +
		data.name +
		", umur saya " +
		data.age +
		" tahun, alamat saya di " +
		data.address +
		", dan saya punya hobby yaitu " +
		data.hobby
	);
}

var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3
function hitung_huruf_vokal(nama) {
	var hasil = nama
		.toLowerCase()
		.split("")
		.filter(function (a) {
			return a == "a" || a == "i" || a == "u" || a == "e" || a == "o";
		});
	return hasil.length;
}

var hitung_1 = hitung_huruf_vokal("Muhammad");
var hitung_2 = hitung_huruf_vokal("Iqbal");
console.log(hitung_1, hitung_2);

// soal 4
function hitung(angka) {
	return angka * 2 - 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
